#!/usr/bin/env python

import numpy
from mkfractal import mkfractal

from amuse.datamodel.particles import Particles
from amuse.units import nbody_system

def make_nbody_system(positions):
    particles = Particles(len(positions))
    particles.x = positions[:,0] | nbody_system.length
    particles.y = positions[:,1] | nbody_system.length
    particles.z = positions[:,2] | nbody_system.length
    return particles

fractals = (mkfractal(500, fdim=2.0) for i in range(100))
systems = (make_nbody_system(f['position']) for f in fractals)
bcds = [s.box_counting_dimension() for s in systems]

print(bcds)
print(numpy.mean(bcds))




