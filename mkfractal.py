#!/usr/bin/env python

import argparse
import numpy

#int float int -> dict
def mkfractal(nstars, fdim=1.8, ndiv=2, dim=3):
    if not (1.0 <= fdim <= dim):
        raise Exception('Fractal dimension must be between 1.0 and {}'.format(dim))

    fractal = new_fractal(fdim, ndiv, dim)

    while fractal['size'] < nstars:
        fractal = add_generation(fractal)

    nodes = tree_to_nodes(fractal['tree'])
    print(len(nodes))
    nodes_final = numpy.random.choice(nodes, size=nstars, replace=False)

    print(len(nodes_final), len(set(nodes_final)))

    fractal['nodes'] = nodes_final

    positions = nodes_to_positions(nodes_final, ndiv, fractal['generations'],
                                   fractal['dim'])
    fractal['position'] = numpy.array(positions)
    return fractal

# dict -> dict
def add_generation(fractal):
    """ Add children to youngest generation """
    next_gen = []
    counter = 0

    while counter == 0:

        for child in fractal['last_gen']:
            sample = numpy.random.random(fractal['nsubs'])
            bools = sample < fractal['probability']
            parent = dict(enumerate([{} if i else None for i in bools]))
            counter += bools.sum()

            #The child node becomes a parent node
            fractal['tree'][child] = parent

            #and the parent's children are the next generation.
            for key in parent:
                if parent[key] is not None:
                    next_gen.append(child + (key,))

        if counter == 0 :
            print('No children matured, drawing new sample')


    fractal['last_gen'] = next_gen
    fractal['generations'] +=1
    fractal['size'] += counter
    return fractal

#float int -> dict
def new_fractal(fdim, ndiv, dim):
    fractal = { 'size'        : 1
              , 'generations' : 1
              , 'fdim'        : fdim
              , 'ndiv'        : ndiv
              , 'dim'         : dim
              , 'nsubs'       : ndiv**dim
              , 'probability' : ndiv**(fdim - dim)
              , 'tree'        : {():{}} #The root node
              , 'last_gen'    : [()]
              , 'nodes'       : None
              , 'position'    : None
              }
    return fractal

# [tuple] int int -> ndarray
def nodes_to_positions(nodes, ndiv, generations, dim):

    sides = [1.0/(ndiv**i) for i in range(generations)]
    centers = [get_centers(s, ndiv) for s in sides]

    # tuple -> ndarray
    def node_to_point(node, ndiv, centers):
        point = numpy.zeros(dim)
        for generation, nr in enumerate(node):
            indices = nr_to_indices(nr, ndiv, dim)
            p = numpy.array([centers[generation][index] for index in indices])
            point = point + p
        return point

    positions = [node_to_point(node, ndiv, centers) for node in nodes]
    return positions

def get_centers(side, ndiv):
    """
    >>> get_centers(1, ndiv=2)
    array([-0.25,  0.25])
    >>> get_centers(1, ndiv=3)
    array([-0.33333333,  0.        ,  0.33333333])
    >>> get_centers(1, ndiv=4)
    array([-0.375, -0.125,  0.125,  0.375])

    Illustrated example with ndiv=3
    -------------------------------

    -side/2       0         +side/2

      |           |           |
      |           |           |
      v           v           v

      -------------------------
      |   |   |   |   |   |   |
      |   c   |   c   |   c   |
      |   |   |   |   |   |   |
      -------------------------
          ^       ^       ^
          |       |       |
          |       |       |
         [0]     [1]     [2]  <--- centers

    """
    assert ndiv > 1
    arr = numpy.linspace(-side/2.0, side/2.0, 2*ndiv + 1)
    centers = uneven_index(arr)
    return centers

# dict -> list
def tree_to_nodes(tree, sort=False):
    nodes = []
    for key in tree:
        nodes.append(key)
        children = [k for k in tree[key] if tree[key][k] == {}]
        for c in children:
            nodes.append(key + (c,))

    if sort:
        return sorted(list(set(nodes)))
    else:
        return list(set(nodes))

# int int int -> (ints)
def nr_to_indices(nr, ndiv, dim):
    indices = []
    subspace = dim
    while subspace > 0:
        subspace -= 1
        q, rem = divmod(nr, ndiv**subspace)
        indices.append(q)
        nr = rem
    return indices

# ndarray -> ndarray
def uneven_index(arr):
    return arr[1::2]

def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ndiv', default=2, type=int)
    parser.add_argument('--fdim', default=1.8, type=float)
    parser.add_argument('--size', default=100, type=int)
    parser.add_argument('--dim', default=3, type=int)
    return parser.parse_args()

if __name__ == "__main__":
    args = get_arguments()
    fractal = mkfractal(args.size, args.fdim, args.ndiv, args.dim)

    print(fractal['position'])


