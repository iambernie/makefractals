#!/usr/bin/env python

import numpy
from mkfractal import mkfractal
import matplotlib.pyplot as plt

def translate_to_positive(positions):
    return positions - positions.min(axis=0)

def scale_to_one(positions):
    return positions / positions.max()

def number_of_boxes_filled(pos, eps):
    return len(set([(r[0], r[1], r[2]) for r in (pos * eps).astype(int)])) 
    
def minkowski_dimension(positions):
    pos = scale_to_one(translate_to_positive(positions))
    epsilons = range(1, 128)
    N = [number_of_boxes_filled(pos, e) for e in epsilons] 
    fit_coefficients = numpy.polyfit(numpy.log(epsilons), numpy.log(N), 1)
    D = 1.0/fit_coefficients[0]
    return D, N, epsilons

fractals = (mkfractal(500, fdim=2.8) for i in range(100))

bcds = []
for i, f in enumerate(fractals):

    pos = scale_to_one(translate_to_positive(f['position']))
    epsilons = range(1, 32)
    #epsilons = (2**numpy.arange(1.0, 7.0, 0.05)).round()
    N = [number_of_boxes_filled(pos, e) for e in epsilons] 

    upper_index = numpy.searchsorted(N, 0.2 * len(pos))

    a1, a0 = numpy.polyfit(numpy.log(epsilons[:upper_index]), numpy.log(N[:upper_index]), 1)
    D = a1

    x = numpy.linspace(numpy.log(epsilons).min(), numpy.log(epsilons).max(), 10)
    y = a0 + a1*x
    plt.scatter(numpy.log(epsilons), numpy.log(N)) 
    plt.plot(x, y, c='r') 
    plt.savefig('images/'+str(i)+'.png')

    plt.clf()

    bcds.append(D)

print(bcds)
print(numpy.mean(bcds))

