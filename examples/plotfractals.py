#!/usr/bin/env python

import uuid
import argparse
import matplotlib.pyplot as plt
from fractalcluster import FractalCluster

def main(size, fdim, ndiv, dim, output, dotsize, dotcolor, grid, suffix):

    f = FractalCluster(size, fdim, ndiv, dim)
    pos = f.positions

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)
    ax.scatter(pos[:,0], pos[:,1], s=dotsize, c=dotcolor)
    ax.set_title('Size={} fdim={} ndiv={}'.format(len(pos), f.fdim, f.ndiv))
    ax.set_xlabel('x')
    ax.set_ylabel('y')

    if grid:
        ax.grid()

    if output:
        if output != 'auto':
            plt.savefig(output, bbox_inches='tight')
        else:
            # get pretty name from arguments
            trunc_uuid = str(uuid.uuid4())[:8]
            s = "_".join(str(i) for i in [size, fdim, ndiv, dim, trunc_uuid])
            plt.savefig(s+".png", bbox_inches='tight')
    else:
        plt.show()


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--output', '-o')
    parser.add_argument('--suffix')
    parser.add_argument('--grid', action='store_true')
    parser.add_argument('--dim', default=2, type=int)
    parser.add_argument('--fdim', default=1.6, type=float)
    parser.add_argument('--ndiv', default=2, type=int)
    parser.add_argument('--size', default=1000, type=int)
    parser.add_argument('--dotsize', default=10, type=int)
    parser.add_argument('--dotcolor', default='k')
    return parser.parse_args()

if __name__ == '__main__':
    args = get_arguments()
    args_as_dict = vars(args)
    main(**args_as_dict)
