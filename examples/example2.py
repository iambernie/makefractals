#!/usr/bin/env python

from fractalcluster import FractalCluster
import matplotlib.pyplot as plt

sizes = [100, 1000, 2500, 5000]

fdim = 1.6
ndiv = 5

fractals = [FractalCluster(s, fdim=fdim, ndiv=ndiv, dim=2) for s in sizes]

fig = plt.figure(figsize=(10, 10))
ax1 = fig.add_subplot(221)
ax2 = fig.add_subplot(222)
ax3 = fig.add_subplot(223)
ax4 = fig.add_subplot(224)

axes = [ax1, ax2, ax3, ax4]

for f, ax in zip(fractals, axes):
    pos = f.positions
    ax.scatter(pos[:,0], pos[:,1], s=1)
    ax.set_title('Size={} fdim={} ndiv={}'.format(len(pos), f.fdim, f.ndiv))
    ax.set_xlabel('x')
    ax.set_ylabel('y')

plt.savefig('moresuchfractals.png')
